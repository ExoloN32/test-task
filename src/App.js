import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';
import Color from './Components/Color'
import Montage from './Components/Montage'
import Header from './Components/Header'
import './App.css'
 
class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {tab: 5, class: ["tab", "tab", "tab", "tab", "tab", "tab focus", "tab red", "tab red"]};
        this.info = this.info.bind(this);
    }

    setTab(val) {
        this.setState({tab: val})
        this.setClass(val);
    }

    setClass(val) {
        let newClass = [];
        for(let i=0; i<8; ++i) {
            if(val === i) {
                newClass[i] = "tab focus";
            }
            else if(val < i) {
                newClass[i] = "tab red";
            }
            else {
                newClass[i] = "tab";
            }
        }
        this.setState({class: newClass});
    }

    info() {
        this.setClass(6);
    }

    render() {
        return (
            <Router className="body">
                
                    <Header/>
                    <div className="main">
                        <Switch>
                            <Route exact path="/" render={props => <Color func={this.info} {...props}/>}/>
                            <Route path="/montage" component={Montage}/>
                        </Switch>
                    </div>
                
                <div className="footer">
                    <div className={this.state.class[0]} onClick={() => this.setTab(0)}>Вариант кухни</div>
                    <div className={this.state.class[1]} onClick={() => this.setTab(1)}>Размеры</div>
                    <div className={this.state.class[2]} onClick={() => this.setTab(2)}>Сенсор</div>
                    <div className={this.state.class[3]} onClick={() => this.setTab(3)}>Питающий кабель</div>
                    <div className={this.state.class[4]} onClick={() => this.setTab(4)}>Блок питания</div>
                    <Link to="/" className={this.state.class[5]} onClick={() => this.setTab(5)}>Цвет свечения</Link>
                    <Link to="/montage" className={this.state.class[6]} onClick={() => this.setTab(6)}>Монтаж</Link>
                    <div className={this.state.class[7]} onClick={() => this.setTab(7)}>Корзина</div>
                </div>
            </Router>
        );
    }
}

export default App;
