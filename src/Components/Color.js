import React from 'react';
import './Color.css';
import Icon from '@mdi/react';
import { mdiInformationVariant, mdiCheck } from '@mdi/js';
import { BrowserRouter as Router, Link } from 'react-router-dom';


class Color extends React.Component {
    constructor (props) {
        super(props);
        this.state = {slide: "/img/Photo-4.jpg", mode: 0, color: 0};
    }

    photo = [
        "/img/Photo-4.jpg",
        "/img/Photo-5.jpg",
        "/img/Photo-6.jpg"
    ];

    setSlide(val) {
        this.setState({slide: this.photo[val], mode: val});
    }

    setColor(val) {
        this.setState({color: val});
        this.setSlide(val);
    }

    render () {
        return (
            <div className="main_wrap">
                <div className="block first">
                    <div className="slider">
                        <img src={this.state.slide} width="100%" alt="Цвет"/>
                        <div className="points">
                            <div className={(this.state.mode===0)?"point act":"point"} onClick={() => this.setSlide(0)}></div>
                            <div className={(this.state.mode===1)?"point act":"point"} onClick={() => this.setSlide(1)}></div>
                            <div className={(this.state.mode===2)?"point act":"point"} onClick={() => this.setSlide(2)}></div>
                        </div>
                    </div>
                    
                </div>
                <div className="block">
                    <div className="character">
                        <div className="chank">
                            <div className="chank_left">Класс:</div>
                            <div className="chank_right"><span>Standart</span></div>
                        </div>
                        <div className="chank">
                            <div className="chank_left">Потребляемая мощность:</div>
                            <div className="chank_right">59Вт.</div>
                        </div>
                        <div className="chank">
                            <div className="chank_left">Сила света:</div>
                            <div className="chank_right">3459 Люмен = 7,5 ламп накаливания по 40Вт.</div>
                        </div>
                        <div className="chank">
                            <div className="chank_left">Гарантия:</div>
                            <div className="chank_right">3года.</div>
                        </div>
                        <div className="chank">
                            <div className="chank_left">Монтаж:</div>
                            <div className="chank_right">Да.</div>
                        </div>
                        <div className="chank">
                            <div className="chank_left">Итого сумма:</div>
                            <div className="chank_right">2594 рублей.</div>
                        </div>
                    </div>
                    <div className="line">
                            <Link to="/montage" className="btn_info" onClick={() => this.props.func()}>
                                <Icon path={mdiInformationVariant}
                                    title="Info"
                                    size={1.6}
                                    horizontal
                                    vertical
                                    rotate={180}
                                    color="white"
                                />
                            </Link>
                            <div className="head">
                                Выберите цвет свечения
                            </div>
                    </div>
                    <div className="slide_container">
                        <div className="slide_item">
                            <img 
                                className={(this.state.color===0)?"slide_img checked":"slide_img"} 
                                src="/img/Photo-4.jpg"
                                onClick={() => this.setColor(0)}
                                alt="Холодный" />
                            <div className={(this.state.color===0)?"checkBox checked":"checkBox"}>
                                <Icon path={mdiCheck}
                                    size={0.8}
                                    horizontal
                                    vertical
                                    rotate={180}
                                    color="white"
                                />
                            </div>
                            <div className="name">Холодный</div>
                        </div>
                        <div className="slide_item">
                            <img 
                                className={(this.state.color===1)?"slide_img checked":"slide_img"} 
                                src="/img/Photo-5.jpg"
                                onClick={() => this.setColor(1)}
                                alt="Дневной" />
                            <div className={(this.state.color===1)?"checkBox checked":"checkBox"}>
                                <Icon path={mdiCheck}
                                    size={0.8}
                                    horizontal
                                    vertical
                                    rotate={180}
                                    color="white"
                                />
                            </div>
                            <div className="name">Дневной</div>
                        </div>
                        <div className="slide_item">
                            <img 
                                className={(this.state.color===2)?"slide_img checked":"slide_img"} 
                                src="/img/Photo-6.jpg"
                                onClick={() => this.setColor(2)}
                                alt="Теплый" />
                            <div className={(this.state.color===2)?"checkBox checked":"checkBox"}>
                                <Icon path={mdiCheck}
                                    size={0.8}
                                    horizontal
                                    vertical
                                    rotate={180}
                                    color="white"
                                />
                            </div>
                            <div className="name">Теплый</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Color;