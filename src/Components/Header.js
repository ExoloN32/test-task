import React from 'react';
import './Header.css';
import Icon from '@mdi/react';
import { mdiCartOutline, mdiMenu } from '@mdi/js';

class Header extends React.Component {
    render() {
        return(
            <div className="header">
                <div className="wrap">
                    <div>
                        <img src="/img/logo.png" alt="logo"></img>
                    </div>
                    <div>
                        <Icon path={mdiCartOutline}
                            title="Cart"
                            size={1}
                            horizontal
                            vertical
                            rotate={180}
                            color="white"
                        />
                        <div className="chip">1</div>
                        <Icon className="hamburger"
                            path={mdiMenu}
                            title="Menu"
                            size={1.2}
                            horizontal
                            vertical
                            rotate={180}
                            color="white"
                        />
                    </div>
                </div>
            </div>
        )
    }
}

export default Header